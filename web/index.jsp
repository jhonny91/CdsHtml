<%-- 
    Document   : index
    Created on : 18-dic-2017, 17:53:42
    Author     : jhonny
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" type="text/css" href="estilos.css">
    </head>
    <body>
        <header>
		<nav>
			<ul class="menu">
				<a href="index.jsp?table=cd"><li>CD</li></a>
				<a href="index.jsp?table=genero"><li>Género</li></a>
				<a href="index.jsp?table=idioma"><li>Idioma</li></a>
				<a href="index.jsp?table=persona"><li>Persona</li></a>
				<a href="index.jsp?table=cantante"><li>Cantante</li></a>
				<a href="index.jsp?table=discografica"><li>Discográfica</li></a>
			</ul>
		</nav>
	</header>
	<div class="consultas">
        <% String tabla = "cd";
                if( request.getParameter("table") != null){
                    tabla = request.getParameter("table");
                }
            
         %>
            
         <% if(tabla.equals("cd")){ %>
	<ul class="consultas">
		<a href="insertCd.jsp"><li>Insertar</li></a>
		<a href="listar.jsp"><li>Listar</li></a>
		<a href=""><li>Editar</li></a>
	</ul>
        <h3 style="float: left;">CD</h3>
         <% }else if(tabla.equals("genero")){ %>
         <ul class="consultas">
		<a href="insertGenero.jsp"><li>Insertar</li></a>
		<a href="listar.jsp"><li>Listar</li></a>
		<a href=""><li>Edit</li></a>
	</ul>
         <h3 style="float: left;">GENERO</h3>
         <% }else if(tabla.equals("idioma")) { %>
              <ul class="consultas">
		<a href="insertIdioma.jsp"><li>Insertar</li></a>
		<a href="listar.jsp"><li>Listar</li></a>
		<a href=""><li>Edit</li></a>
	</ul>
        
             
         <% } %>
	  </body>
</html>
