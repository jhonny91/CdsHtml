/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coleccioncds;

import java.util.Random;

/**
 *
 * @author Grupo2
 */
public class Persona {

    private int id = 0;
    private String nombre;
    private String apellido1;
    private String apellido2 = "";
    private String dni;
    private char sexo;

    public Persona(String nombre, String apellido1, String apellido2, char sexo) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.sexo = sexo;
        this.dni = this.generaDNI();
    }

    public Persona(String nombre, String apellido1, char sexo) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.sexo = sexo;
        this.dni = this.generaDNI();
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido1() {
        return apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getDni() {
        return dni;
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    private String generaDNI() {
        String dni;
        
        Random r1 = new Random();
        int num = r1.nextInt(99999999);

        String[] letra = {"T", "R", "W", "A", "G", "M", "Y", "G", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"};
        int pos = num%22;
        
        dni = String.valueOf(num) + letra[pos];
        
        return dni;
    }
    
    public void setDni(String dni) {
        this.dni = dni;
    }

}
