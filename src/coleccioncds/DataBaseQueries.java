/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coleccioncds;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Grupo2
 */
public class DataBaseQueries {
    
    private static Connection conn;
    private static Statement st;
    private static ResultSet resultado;
    
    public static Persona selectPersona(int id) throws SQLException, ClassNotFoundException{
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection("jdbc:mysql://localhost/coleccioncds", "root", "");
            String sql = "SELECT * FROM personas WHERE id ="+ id;

           

            st = conn.createStatement();
            resultado = st.executeQuery(sql);

            String str=null;
            Persona persona=null;
            while(resultado.next()){
                
            persona = new Persona(resultado.getString("nombre"), resultado.getString("apellido1"), resultado.getString("apellido2"), resultado.getString("sexo").charAt(0));
            persona.setDni(resultado.getString("dni"));
            }
            System.out.println("error");
            
            

            conn.close();
            return persona;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DataBaseQueries.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static ArrayList<Persona> selectPersonas() throws SQLException, ClassNotFoundException{
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/coleccioncds", "root", "");
            String sql = "SELECT * FROM personas";
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            ArrayList<Persona> personas = new ArrayList<>();
            while(resultado.next()){
                Persona persona = new Persona(resultado.getString("nombre"), resultado.getString("apellido1"), resultado.getString("apellido2"), resultado.getString("sexo").charAt(0));
                persona.setId(resultado.getInt("id"));
                persona.setDni(resultado.getString("dni"));
                personas.add(persona);
            }
            conn.close();
            return personas;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DataBaseQueries.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static void insertPersona(Persona persona) throws SQLException, ClassNotFoundException{
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/coleccioncds", "root", "");
            String query = "INSERT INTO personas VALUES("
                    + null + ", "
                    + "\"" + persona.getNombre() + "\", "
                    + "\"" + persona.getApellido1() + "\", "
                    + "\"" + persona.getApellido2() + "\", "
                    + "\"" + persona.getSexo() + "\", "
                    + "\"" + persona.getDni() + "\")";
            st = conn.createStatement();
            st.executeUpdate(query);
            System.out.println("Persona registrada con éxito");
            conn.close();
        }catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Error en el almacenamiento de datos");
        }
    }
    
    public static void deletePersona(int id) throws SQLException, ClassNotFoundException{
        try{
            Class.forName("com.mysql.jdbc.Driver");

          

            conn = DriverManager.getConnection("jdbc:mysql://localhost/coleccioncds", "root", "Lenovo1");
            String query = "DELETE FROM personas WHERE id = " + id;

            st = conn.createStatement();
            st.executeUpdate(query);
            System.out.println("Persona borrada con éxito");
            conn.close();
        }catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Error en el borrado de datos");
        }
    }
    
    public static void updatePersona(Persona persona, int id) throws SQLException, ClassNotFoundException{
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/coleccioncds", "root", "");
            String query = "UPDATE personas SET "
                    + "nombre = \"" + persona.getNombre()

                    + "\" ,apellido1 = \"" + persona.getApellido1()
                    + "\" ,apellido2 = \"" + persona.getApellido2()
                    + "\" ,sexo = \"" + persona.getSexo()
                    + "\" ,dni =\"" + persona.getDni()+"\""
                    + " WHERE id = " + id;
            System.out.println(query);

            st = conn.createStatement();
            st.executeUpdate(query);
            System.out.println("Datos de la persona actualizados con ´éxito");
            conn.close();
        }catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Error en la actualización de datos");
        }
    }
    
    public static Cd selectCd(int id) throws SQLException, ClassNotFoundException{
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/coleccioncds", "root", "Lenovo1");
            String sql = "SELECT * FROM cds WHERE id = " + id;
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            Cd cd = new Cd(resultado.getString("titulo"), resultado.getInt("genero"), resultado.getString("autor"), resultado.getString("año"), resultado.getInt("persona"), resultado.getInt("idioma"));
            cd.setId(resultado.getInt("id"));
            conn.close();
            return cd;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DataBaseQueries.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static ArrayList<Cd> selectCds() throws SQLException, ClassNotFoundException{
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/coleccioncds", "root", "Lenovo1");
            String sql = "SELECT * FROM cds";
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            ArrayList<Cd> cds = new ArrayList<>();
            while(resultado.next()){
                Cd cd = new Cd(resultado.getString("titulo"), resultado.getInt("genero"), resultado.getString("autor"), resultado.getString("año"), resultado.getInt("persona"), resultado.getInt("idioma"));
                cd.setId(resultado.getInt("id"));
                cds.add(cd);
            }
            conn.close();
            return cds;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DataBaseQueries.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static void insertCd(Cd cd) throws SQLException, ClassNotFoundException{
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/coleccioncds", "root", "Lenovo1");
            String query = "INSERT INTO cds VALUES("
                    + null + ", "
                    + "\"" + cd.getTitulo() + "\", "
                    + "\"" + cd.getGenero() + "\", "
                    + "\"" + cd.getAutor()+ "\", "
                    + "\"" + cd.getAño()+ "\", "
                    + "\"" + cd.getPersona()+ "\", "
                    + "\"" + cd.getIdioma()+ "\")";
            st = conn.createStatement();
            st.executeUpdate(query);
            System.out.println("cd registrado con éxito");
            conn.close();
        }catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Error en el almacenamiento de datos");
        }
    }
    
    public static void deleteCd(int id) throws SQLException, ClassNotFoundException{
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/coleccioncds", "root", "Lenovo1");
            String query = "DELETE FROM cds WHERE id = " + id;
            st = conn.createStatement();
            st.executeUpdate(query);
            System.out.println("cd borrado con éxito");
            conn.close();
        }catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Error en el borrado de datos");
        }
    }
    
    public static void updateCd(Cd cd, int id) throws SQLException, ClassNotFoundException{
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/coleccioncds", "root", "Lenovo1");
            String query = "UPDATE cds SET "
                    + "titulo = \"" + cd.getTitulo()
                    + "genero = \"" + cd.getGenero()
                    + "autor = \"" + cd.getAutor()
                    + "año = \"" + cd.getAño()
                    + "persona = \"" + cd.getPersona()
                    + "idioma = \"" + cd.getIdioma()
                    + "\"" + " WHERE id = " + id ;
            st = conn.createStatement();
            st.executeUpdate(query);
            System.out.println("Datos del cd actualizados con éxito");
            conn.close();
        }catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Error en la actualización de datos");
        }
    }
    
    public static ArrayList<Genero> selectGeneros() throws SQLException, ClassNotFoundException{
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/coleccioncds", "root", "Lenovo1");
            String sql = "SELECT * FROM generos";
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            ArrayList<Genero> generos = new ArrayList<>();
            while(resultado.next()){
                Genero genero = new Genero(resultado.getString("nombre"));
                genero.setId(resultado.getInt("id"));
                generos.add(genero);
            }
            conn.close();
            return generos;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DataBaseQueries.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static ArrayList<Idioma> selectIdiomas() throws SQLException, ClassNotFoundException{
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost/coleccioncds", "root", "Lenovo1");
            String sql = "SELECT * FROM idiomas";
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            ArrayList<Idioma> idiomas = new ArrayList<>();
            while(resultado.next()){
                Idioma idioma = new Idioma(resultado.getString("nombre"));
                idioma.setId(resultado.getInt("id"));
                idiomas.add(idioma);
            }
            conn.close();
            return idiomas;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DataBaseQueries.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
